#include "common.h"
#include "tcp_client.h"
#include <iostream>

/**
 * @brief send handler function that is called after sending is done
 * @param ec - error code
 * @param send_count - send count
 * @return -
 */
void sendHandler(const boost::system::error_code &ec, std::size_t send_count);

/**
 * @brief receive handler function that is called after receiving is done
 * @param ec - error code
 * @param recv_count - receive count
 * @return -
 */
void receiveHandler(const boost::system::error_code &ec, std::size_t recv_count);


TCPClient::TCPClient(void)
  : m_socket(m_context),
    m_ip(INVALID_IP_ADDRESS),
    m_port(INVALID_PORT_NUMBER),
    m_is_started(false)
{

}

TCPClient::~TCPClient() {
  if (m_socket.is_open()) {
    m_socket.close();
  }

  m_ip = INVALID_IP_ADDRESS;
  m_port = INVALID_PORT_NUMBER;
}

void TCPClient::setIP(const std::string &ip) {
  if (MIN_IPV4_LENGTH > ip.length() || MAX_IPV4_LENGTH < ip.length() ) {
    std::cerr << "Invalid IP address length" << std::endl;
  }
  else if (m_is_started) {
    std::cerr << "Could not set IP address when client is running" << std::endl;
  }
  else {
    m_ip = ip;
  }

  return;
}

std::string TCPClient::getIP(void) const {
  return m_ip;
}

void TCPClient::setPort(const unsigned short port) {
  if (INVALID_PORT_NUMBER == port) {
    std::cerr << "Invalid port number" << std::endl;
  }
  else if (m_is_started) {
    std::cerr << "Could not set port when client is running" << std::endl;
  }
  else {
    m_port = port;
  }

  return;
}

unsigned short TCPClient::getPort(void) const {
  return m_port;
}

bool TCPClient::start(void) {
  if (INVALID_IP_ADDRESS == m_ip) {
    std::cerr << "The IP address must be set before start" << std::endl;    
    return false;
  }

  if (INVALID_PORT_NUMBER == m_port) {
    std::cerr << "The port number must be set before start" << std::endl;
    return false;
  }

  boost::system::error_code ec;
  ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(m_ip), m_port);

  m_socket.connect(endpoint, ec);

  // check whether socket connection is done or not
  if (0 != ec.value()) {
    std::cerr << "Could not connect to server" << std::endl;

    m_is_started = false;
  }
  else {
    m_is_started = true;
  }
    
  return m_is_started;
}

void TCPClient::stop(void) {
  if (m_socket.is_open()) {
    m_socket.close();
  }

  m_is_started = false;

  return;
}

int TCPClient::receive(unsigned char *data, const int len) {
  // if length is invalid or socket is closed, do not continue  
  if (TRANSCEIVE_SIZE != len || !m_socket.is_open()) {
    return INVALID_TRANSCEIVE;
  }

  if (TRANSCEIVE_SIZE == m_socket.available()) {
    m_socket.async_receive(buffer(data, len), receiveHandler);
    // poll and reset to call receiveHandler
    m_context.poll();
    m_context.reset();
  }
  else {
    // if there is no server, that means connection is done, then close socket 
    m_socket.close();
  }
  
  return len;
}

int TCPClient::send(const unsigned char *data, const int len) {
  // if length is invalid or socket is closed, do not continue
  if (TRANSCEIVE_SIZE != len || !m_socket.is_open()) {
    return INVALID_TRANSCEIVE;
  }

  m_socket.async_send(buffer(data, len), sendHandler);
  // poll and reset to call sendHandler
  m_context.poll();
  m_context.reset();

  return len;
}

bool TCPClient::isActive(void) {
  if (m_is_started) {
    return m_socket.is_open();
  }

  // if the client hasn't been started yet, return false
  return false;
}


void receiveHandler(const boost::system::error_code &ec, std::size_t recv_count) {
#if DEBUG_FLAG
  if (0 == ec.value() && TRANSCEIVE_SIZE == recv_count) {
    std::cout << "Data (" << recv_count << " bytes) is received" << std::endl;
  }
#endif

  return;
}


void sendHandler(const boost::system::error_code &ec, std::size_t send_count) {
#if DEBUG_FLAG
  if (0 == ec.value() && TRANSCEIVE_SIZE == send_count) {
    std::cout << "Data (" << send_count << " bytes) is sent" << std::endl;
  }
#endif

  return;
}
