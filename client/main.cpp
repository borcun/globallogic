#include "tcp_client.h"
#include "common.h"
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp>
#include <boost/asio.hpp>

/**
 * @brief function that display application usage
 * @return -
 */
void usage(void) {
  std::cout << "usage: ./tcp_client <server ip> <server port>" << std::endl;

  return;
}

int main(int argc, char **argv) {
  if (3 != argc) {
    usage();

    return -1;
  }
  
  TCPClient client;

  srand(time(NULL));
  
  client.setIP(argv[1]);
  client.setPort(atoi(argv[2]));
  
  if (!client.start()) {
    return -1;
  }

  std::cout << "Connection is established" << std::endl;

  // during client is active, continue transceiving
  while (client.isActive()) {
    // get random number in range
    unsigned int num = (MIN_DATA_LIMIT + (rand() % (MAX_DATA_LIMIT - MIN_DATA_LIMIT)));
    unsigned char tx_data[TRANSCEIVE_SIZE] = {0x00};

    // change endianness of num when transferring to byte array
    for (int i = 0; i < TRANSCEIVE_SIZE; ++i) {
      tx_data[i] = (unsigned char) (num >> (8 * i));
    }
    
    // if sending is OK, then pass receiving operation
    if (TRANSCEIVE_SIZE == client.send(tx_data, TRANSCEIVE_SIZE)) {
      boost::this_thread::sleep_for(boost::chrono::milliseconds(TRANSCEIVE_PERIOD));
    
      unsigned char rx_data[TRANSCEIVE_SIZE] = {0x00};

      if (TRANSCEIVE_SIZE != client.receive(rx_data, TRANSCEIVE_SIZE)) {
	std::cerr << "Could not receive ACK" << std::endl;
      }
    }    
  }

  client.stop();

  std::cout << "Connection is closed" << std::endl;

  return 0;
}
