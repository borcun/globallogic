/**
 * @file tcp_client.h
 * @author B. Orcun OZKABLAN
 *
 * The file that contains TCP client functionalities
 */

#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include <string>
#include <boost/asio.hpp>

using namespace boost::asio;

/**
 * @class TCPClient that creates and manages a TCP client
 */
class TCPClient
{
public:
  /**
   * @brief default constructor
   */
  TCPClient(void);
  
  /**
   * @brief destructor
   */
  virtual ~TCPClient();
  
  /**
   * @brief function that sets IP address of TCP server which the client want to connect
   * @param ip - server IP address
   * @return -
   */
  void setIP(const std::string &ip);

  /**
   * @brief function that gets server IP address
   * @return server IP address
   */
  std::string getIP(void) const;

  /**
   * @brief function that sets port number of TCP server which the client want to connect
   * @param port - server port number
   * @return -
   */
  void setPort(const unsigned short port);

  /**
   * @brief function that gets port number
   * @return port number
   */
  unsigned short getPort(void) const;

  /**
   * @brief function that starts TCP client
   * @return true if the client starts. Otherwise, return false.
   */
  bool start(void);

  /**
   * @brief function that stops TCP client
   * @return -
   */
  void stop(void);

  /**
   * @brief function that receives data from server
   * @param data - received data stream
   * @param len - length of received data
   * @return length of data if receiving is success. Otherwise, return -1.
   */
  int receive(unsigned char *data, const int len);

  /**
   * @brief function that sends data to server
   * @param data - sent data stream
   * @param len - length of sent data
   * @return length of data if sending is success. Otherwise, return -1.
   */
  int send(const unsigned char *data, const int len);

  /**
   * @brief function that gets client state
   * @return true if client is active. Otherwise, return false.
   */
  bool isActive(void);
  
private:
  //! io context
  io_context m_context;

  //! socket
  ip::tcp::socket m_socket;

  //! server IP address
  std::string m_ip;
  
  //! server port number
  unsigned short m_port;

  //! client state
  bool m_is_started;
};

#endif
