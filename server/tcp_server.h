/**
 * @file tcp_server.h
 * @author B. Orcun OZKABLAN
 *
 * The file that contains TCP server functionalities
 */

#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include "connection_object.h"
#include <vector>

using namespace boost::asio;

/**
 * @class TCPServer that creates and manages a TCP server
 */
class TCPServer
{
public:
  /**
   * @brief function that gets instance of class
   * @remark TCP Server is implemented with using Singleton Pattern
   * @return instance of class
   */
  static TCPServer *getInstance(void);

  /**
   * @brief destructor
   */
  virtual ~TCPServer();
  
  /**
   * @brief function that sets port number of TCP server
   * @remark be sure that port number isn't used by any application
   * @param port - port number of TCP server
   * @return -
   */
  void setPort(const unsigned short port);

  /**
   * @brief function that gets port number
   * @return port number
   */
  unsigned short getPort(void) const;

  /**
   * @brief function that starts TCP server
   * @return true if the server starts. Otherwise, return false.
   */
  bool start(void);

  /**
   * @brief function that stops TCP server
   * @return -
   */
  void stop(void);

  /**
   * @brief function that accepts connection from client-side
   * @return -
   */
  void accept(void);

private:
  //! single instance of class
  static TCPServer *m_self;

  //! IO Context instance
  io_context m_context;

  //! port number
  unsigned short m_port;

  //! server state
  bool m_is_started;

  //! connection object list
  std::vector<ConnectionObject *> m_conn_objs;

  /**
   * @brief default constructor
   */
  TCPServer(void);  
};

#endif
