#include "connection_object.h"
#include "common.h"
#include "accumulator.h"
#include <iostream>
#include <boost/chrono.hpp>
#include <boost/thread/mutex.hpp>

/**
 * @enum TransceiveState that contains transceive states
 */
enum TransceiveState
{
  //! none
  TS_NONE = 0x00,

  //! state for success receiving
  TS_RECEIVED = 0x01,

  //! state for success sending
  TS_SENT = 0x02
};

//! transceive state
TransceiveState ts = TS_NONE;

void connectionHandler(ConnectionObject *conn_obj) {
  if (nullptr == conn_obj->socket || false == conn_obj->is_connected) {
    std::cerr << "Thread function parameter is invalid" << std::endl;

    return;
  }

  while (conn_obj->is_connected) {
    unsigned int rx_data = 0;

    if (TRANSCEIVE_SIZE == conn_obj->socket->available()) {
      conn_obj->socket->async_receive(buffer(&rx_data, TRANSCEIVE_SIZE), receiveHandler);
      // poll and reset to call receiveHandler
      conn_obj->context->poll();
      conn_obj->context->reset();

      // if transceive state is received, that means data is received from client successfully
      if (TS_RECEIVED == ts) {
	unsigned int tx_data = TCP_SERVER_ACK;
      
	conn_obj->socket->async_send(buffer(&tx_data, TRANSCEIVE_SIZE), sendHandler);
	// poll and reset to call sendHandler
	conn_obj->context->poll();
	conn_obj->context->reset();

	// protect accumulation because thred operations may intersect each other
	sum_mutex.lock();
	Accumulator::accumulate(rx_data);
	sum_mutex.unlock();
      }
    }

    // sleep a little time to prevent polling CPU
    boost::this_thread::sleep_for(boost::chrono::milliseconds(TRANSCEIVE_DELAY));
  }
  
  std::cout << "Connection is closed [" << conn_obj->endpoint << "]" << std::endl;

  return;
}

void sendHandler(const boost::system::error_code &ec, std::size_t send_count) {
  // if send operation is OK, set state to TS_SENT
  if (0 == ec.value() && TRANSCEIVE_SIZE == send_count) {
    ts = TS_SENT;
  }
  else {
    ts = TS_NONE;
  }
  
  return;
}

void receiveHandler(const boost::system::error_code &ec, std::size_t recv_count) { 
  // if receive operation is OK, set state to TS_RECEIVED
  if (0 == ec.value() && TRANSCEIVE_SIZE == recv_count) {
    ts = TS_RECEIVED;
  }
  else {
    ts = TS_NONE;
  }

  return;
}
