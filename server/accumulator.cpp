#include "accumulator.h"
#include "common.h"
#include <iostream>

unsigned long int Accumulator::m_data = 0;

Accumulator::Accumulator(void) {
}

Accumulator::~Accumulator() {
}

void Accumulator::setData(const unsigned long int data) {
  m_data = data;

  return;
}

unsigned long int Accumulator::getData(void) {
  return m_data;
}

void Accumulator::accumulate(const unsigned long int data) {
  // if the data is out of range, do nothing
  if (MIN_DATA_LIMIT > data || MAX_DATA_LIMIT < data) {
    return;
  }

  // if sum doesn't reach to limit, process data
  if (TRANSCEIVE_SUM_LIMIT > m_data) {
    if (0 == data % 2) {
      m_data += data;
      
      std::cout << "SUM: " << m_data << std::endl;
    }
    else {
      // prevent negative result for an unsigned variable
      if (data < m_data) {
	m_data -= data;
	
	std::cout << "SUM: " << m_data << std::endl;
      }
    }
  }

  return;
}
